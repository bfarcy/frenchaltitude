# French Altitude

The goal of this little project is to get the altitude in meters from gps coordinates, within France.

At first thoughts, this is an information easily found on internet. A number of websites propose interfaces using [google map apis](https://developers.google.com/maps/documentation/elevation/intro). However it is not free, [around 5.00 USD per 1000 request](https://developers.google.com/maps/documentation/elevation/usage-and-billing) and the data source is unknown.

The [Open-Elevation](https://github.com/Jorl17/open-elevation) open source project provided a public API but it is not running anymore.

A worldwide available open data source is provided by [CGIAR and NASA](https://cgiarcsi.community/data/srtm-90m-digital-elevation-database-v4-1/).

A website providing a paying api using this datasource, [Elevation API](https://elevation-api.io/) 

In France, another datasource is the BD ALTI 75M, that can be found on the [IGN website](https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html).

## BD ALTI 75M

Digital Elevation Model